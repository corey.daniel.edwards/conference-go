import json
import pika
import django
import os
import sys
from django.core.mail import send_mail


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()


def process_approvals(ch, method, properties, body):
    send_mail()

    parameters = pika.ConnectionParameters(host="rabbitmq")
    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()
    channel.queue_declare(queue="presentation_approvals")
    channel.basic_consume(
        queue="presentation_approvals",
        on_message_callback=process_approvals,
        auto_ack=True,
    )
    channel.start_consuming()
    send_mail(
        "admin@conference.go",
        ["presenter_email"],
        "Your presentation has been accepted",
        "{name}, we're happy to tell you that your presentation {title} has been accepted",
        fail_silently=False,
    )
    print("  Received %r" % body)


def process_rejection(ch, method, properties, body):

    parameters = pika.ConnectionParameters(host="rabbitmq")
    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()
    channel.queue_declare(queue="presentation_rejections")
    channel.basic_consume(
        queue="presentation_rejections",
        on_message_callback=process_rejection,
        auto_ack=True,
    )
    channel.start_consuming()
    send_mail(
        "admin@conference.go",
        ["presenter_email"],
        "Your presentation has been rejected",
        "{name}, we regret to inform you that your presentation {title} has been rejected",
        fail_silently=False,
    )
    print("  Received %r" % body)
